import QtQuick.Controls 2.12
import QtQuick 2.12
import QtQml 2.12
import "../Models"
import "../Scripts/Random.js" as Rand

Page {
    id: root
    property Question currentQustion

    signal nextQustion(bool correct)

    onCurrentQustionChanged: {
        _private.answersReady = false

        var answers = []

        for (var i = 0; i !== currentQustion.answers.length; ++i) {
            answers.push({
                            answer: currentQustion.answers[i],
                            selected: false
                         })
        }

        _private.answers = Rand.randomiseList(answers)
    }

    QtObject {
        id: _private
        property bool answersReady: false
        property var answers: []
    }




    Column {
        anchors.left: parent.left
        anchors.right: parent.right

        Label {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 8
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
            text: currentQustion.text

        }

        Column {
            Repeater {
                ItemDelegate {
                    text: modelData.answer.text
                    checkable: !_private.answersReady
                    checked: modelData.selected
                    background: Rectangle {
                        anchors.fill: parent
                        color: _private.answersReady ?
                                   (checked ? (modelData.answer.correct ? "green" : "red") :
                                              (modelData.answer.correct ? "lightgreen" : "white")) :
                                   checked ? "lightgray" : "white"
                    }
                    onCheckedChanged:  {
                        _private.answers[index].selected = checked
                    }
                }
                model: _private.answers
            }
            Button {
                text: qsTr("Ok")
                visible: !_private.answersReady
                onClicked: {
                    _private.answersReady = true

                }
            }
            Button {
                text: qsTr("Next")
                visible: _private.answersReady
                onClicked: {
                    var correct = true;
                    for (var i = 0; i !== _private.answers.length; ++i) {
                        correct = correct &&
                                (_private.answers[i].selected === _private.answers[i].answer.correct)
                    }

                    nextQustion(correct)
                }
            }
        }
    }
}
