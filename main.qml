import QtQuick 2.12
import QtQuick.Controls 2.12
import "./Models"
import "./Views"

ApplicationWindow {
    id: main
    width: 600
    height: 480
    visible: true
    title: qsTr("Hello World")

    property int correctCounter: 0
    property int incorrectCounter: 0

    header: ToolBar {
        Row {
            spacing: 8
            padding: 16
            Label {
                text: "Correct: %1".arg(correctCounter)
            }
            Label {
                text: "Incorrect: %1".arg(incorrectCounter)
            }
        }
    }

    ExampleQuestions {
        id: myQuestionList
    }


    QuestionView {
        anchors.fill: parent

        currentQustion: myQuestionList.currentQuestion
        onNextQustion: {
            if (correct) {
                correctCounter++;
            } else {
                incorrectCounter++;
            }
            myQuestionList.next()
        }
    }
}
