import "./Models"
import QtQml 2.12

import "./Scripts/Random.js" as Rand

QuestionList {
    id: myQuestionList

    property int currentIndex: Rand.getRandInt(0, questions.length)
    readonly property Question currentQuestion: questions[currentIndex]

    function next() {
        var nextIndex;
        do {
            nextIndex = Rand.getRandInt(0, questions.length);
        } while (currentIndex === nextIndex)
        currentIndex = nextIndex
    }

    Question {
        text: qsTr("x + 10 = 5; x = ?")
        Answer {
            text: "5"
        }
        Answer {
            text: "1"
        }
        Answer {
            text: "-5"
            correct: true
        }
        Answer {
            text: "8"
        }
    }

    Question {
        text: qsTr("x^2 = -1; x = ?")
        Answer {
            text: "1"
        }
        Answer {
            text: "-1"
        }
        Answer {
            text: "i"
            correct: true
        }
    }

    Question {
        text: qsTr("x^2 - 25 = 0; x = ?")
        Answer {
            text: "5"
            correct: true
        }
        Answer {
            text: "25"
        }
        Answer {
            text: "-5"
            correct: true
        }
        Answer {
            text: "0"
        }
    }
}
