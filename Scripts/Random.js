
function getRandInt(min, max) {
    return min + Math.floor(Math.random() * (max - min))
}

function randomiseList(list) {

    var indexes = []
    for (var i = 0; i < list.length; ++i) {
        indexes.push(i)
    }

    var result = []
    while (indexes.length > 0) {
        var index_of_index = getRandInt(0, indexes.length)
        var index = indexes[index_of_index]
        result.push(list[index])
        indexes.splice(index_of_index, 1)
    }
    return result
}
